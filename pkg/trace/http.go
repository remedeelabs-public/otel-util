package trace

import (
	"context"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	apicore "go.opentelemetry.io/otel/api/core"
	apitrace "go.opentelemetry.io/otel/api/trace"
)

//RemoteContextFromRequest returns the remote context stored in the request.
func RemoteContextFromRequest(r *http.Request) (context.Context, error) {
	spanCtx := apicore.SpanContext{TraceFlags: apicore.TraceFlagsSampled}

	traceHeader := r.Header.Get("X-Cloud-Trace-Context")
	traceParts := strings.Split(traceHeader, "/")

	if len(traceParts) > 0 && len(traceParts[0]) > 0 {
		traceIDdecoded, err := hex.DecodeString(traceParts[0])
		if err != nil {
			return apitrace.ContextWithRemoteSpanContext(r.Context(), spanCtx), fmt.Errorf("Error parsing traceID (%s): %v", traceParts[0], err)
		}
		var traceID apicore.TraceID
		copy(traceID[:], traceIDdecoded)
		spanCtx.TraceID = traceID
	}

	if len(traceParts) > 1 && len(traceParts[1]) > 0 {
		spanIDdecoded, err := strconv.ParseUint(traceParts[1], 10, 64)
		if err != nil {
			return apitrace.ContextWithRemoteSpanContext(r.Context(), spanCtx), fmt.Errorf("Error parsing spanID (%s): %v", traceParts[1], err)
		}
		var spanID apicore.SpanID
		binary.BigEndian.PutUint64(spanID[:], spanIDdecoded)
		spanCtx.SpanID = spanID
	}

	return apitrace.ContextWithRemoteSpanContext(r.Context(), spanCtx), nil
}
