module go.remedee.dev/otel-util

go 1.14

require (
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	go.opentelemetry.io/otel v0.7.0
	gopkg.in/yaml.v2 v2.2.7 // indirect
)
