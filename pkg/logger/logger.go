package logger

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"github.com/golang/glog"
	apitrace "go.opentelemetry.io/otel/api/trace"
)

// Entry holds a log entry.
type Entry struct {
	Message  string                 `json:"message"`
	Severity string                 `json:"severity,omitempty"`
	Trace    string                 `json:"logging.googleapis.com/trace,omitempty"`
	SpanID   string                 `json:"logging.googleapis.com/spanId,omitempty"`
	Metadata map[string]interface{} `json:"metadata,omitempty"`
}

// String converts an Entry object to the JSON format expected by Stackdriver.
func (e Entry) String() string {
	if e.Severity == "" {
		e.Severity = "DEFAULT" //The log entry has no assigned severity level.
	}
	out, err := json.Marshal(e)
	if err != nil {
		glog.Errorf("json.Marshal: %v", err)
	}
	return string(out)
}

// EntryOption applies changes to an Entry object.
type EntryOption func(*Entry)

// Logger holds methods for logging to Stackdriver.
type Logger struct {
	ProjectID string
}

// NewEntry creates a new Entry object.
func (logger Logger) NewEntry(ctx context.Context, message string, opts ...EntryOption) Entry {
	spanCtx := apitrace.RemoteSpanContextFromContext(ctx)
	entry := Entry{
		Message:  message,
		Trace:    fmt.Sprintf("projects/%s/traces/%s", logger.ProjectID, spanCtx.TraceID.String()),
		SpanID:   spanCtx.SpanID.String(),
		Metadata: make(map[string]interface{}),
	}
	for _, opt := range opts {
		opt(&entry)
	}
	return entry
}

// Debug logs an entry to Stackdriver with the severity level "DEBUG".
// Debug or trace information.
func (logger Logger) Debug(ctx context.Context, message string, opts ...EntryOption) {
	entry := logger.NewEntry(ctx, message, opts...)
	entry.Severity = "DEBUG"
	log.Println(entry)
}

// Info logs an entry to Stackdriver with the severity level "INFO".
// Routine information, such as ongoing status or performance.
func (logger Logger) Info(ctx context.Context, message string, opts ...EntryOption) {
	entry := logger.NewEntry(ctx, message, opts...)
	entry.Severity = "INFO"
	log.Println(entry)
}

// Notice logs an entry to Stackdriver with the severity level "NOTICE".
// Normal but significant events, such as start up, shut down, or a configuration change.
func (logger Logger) Notice(ctx context.Context, message string, opts ...EntryOption) {
	entry := logger.NewEntry(ctx, message, opts...)
	entry.Severity = "NOTICE"
	log.Println(entry)
}

// Warning logs an entry to Stackdriver with the severity level "WARNING".
// Warning events might cause problems.
func (logger Logger) Warning(ctx context.Context, message string, opts ...EntryOption) {
	entry := logger.NewEntry(ctx, message, opts...)
	entry.Severity = "WARNING"
	log.Println(entry)
}

// Error logs an entry to Stackdriver with the severity level "ERROR".
// Error events are likely to cause problems.
func (logger Logger) Error(ctx context.Context, message string, opts ...EntryOption) {
	entry := logger.NewEntry(ctx, message, opts...)
	entry.Severity = "ERROR"
	log.Println(entry)
}

// Critical logs an entry to Stackdriver with the severity level "CRITICAL".
// Critical events cause more severe problems or outages.
func (logger Logger) Critical(ctx context.Context, message string, opts ...EntryOption) {
	entry := logger.NewEntry(ctx, message, opts...)
	entry.Severity = "CRITICAL"
	log.Println(entry)
}

// Alert logs an entry to Stackdriver with the severity level "ALERT".
// A person must take an action immediately.
func (logger Logger) Alert(ctx context.Context, message string, opts ...EntryOption) {
	entry := logger.NewEntry(ctx, message, opts...)
	entry.Severity = "ALERT"
	log.Println(entry)
}

// Emergency logs an entry to Stackdriver with the severity level "EMERGENCY".
// One or more systems are unusable.
func (logger Logger) Emergency(ctx context.Context, message string, opts ...EntryOption) {
	entry := logger.NewEntry(ctx, message, opts...)
	entry.Severity = "EMERGENCY"
	log.Println(entry)
}
