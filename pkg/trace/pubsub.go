package trace

import (
	"context"
	"encoding/hex"
	"fmt"

	apicore "go.opentelemetry.io/otel/api/core"
	apitrace "go.opentelemetry.io/otel/api/trace"
)

//RemoteContextFromTraceSpan returns a remote context with the given traceID and spanID.
func RemoteContextFromTraceSpan(ctx context.Context, traceID, spanID string) (context.Context, error) {
	spanCtx := apicore.SpanContext{TraceFlags: apicore.TraceFlagsSampled}

	if traceID != "" {
		traceIDdecoded, err := hex.DecodeString(traceID)
		if err != nil {
			return apitrace.ContextWithRemoteSpanContext(ctx, spanCtx), fmt.Errorf("Error parsing traceID (%s): %v", traceID, err)
		}
		var traceID apicore.TraceID
		copy(traceID[:], traceIDdecoded)
		spanCtx.TraceID = traceID
	}

	if spanID != "" {
		spanIDdecoded, err := hex.DecodeString(spanID)
		if err != nil {
			return apitrace.ContextWithRemoteSpanContext(ctx, spanCtx), fmt.Errorf("Error parsing spanID (%s): %v", spanID, err)
		}
		var spanID apicore.SpanID
		copy(spanID[:], spanIDdecoded)
		spanCtx.SpanID = spanID
	}

	return apitrace.ContextWithRemoteSpanContext(ctx, spanCtx), nil
}
